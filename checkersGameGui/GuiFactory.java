package checkersGameGui;

import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class GuiFactory {
	public final static Color CASEBLACK = Color.BLUE;
	public final static Color CASEWHITE = Color.rgb(230, 230, 230);
	public final static PieceShape SHAPE = PieceShape.ARC;
	public final static int SIZE = 10;
	public final static double HEIGHT = 600.0;

	public static Canvas createPiece(atelier1.checkersGameNutsAndBolts.PieceSquareColor pieceColor) {
		return new PieceGui(pieceColor);
	}

	public static Pane createSquare(atelier1.checkersGameNutsAndBolts.PieceSquareColor squareColor) {
		return new SquareGui(squareColor);
	}
}
