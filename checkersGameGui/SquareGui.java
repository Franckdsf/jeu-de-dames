package checkersGameGui;

import atelier1.checkersGameNutsAndBolts.PieceSquareColor;
import javafx.geometry.Insets;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

/**
 * @author francoiseperrin
 * 
 * Classe d'affichage des carrés du damier
 * leur couleur est initialisé par les couleurs par défaut du jeu
 *
 */
class SquareGui extends BorderPane  {

    private static int square_counter = 0;
    private int squareid;

    SquareGui(PieceSquareColor squareColor){
        this.squareid = SquareGui.square_counter;
        // increment square id
        SquareGui.square_counter += 1;

        // la couleur est définie par les valeurs par défaut de configuration
        Color color = atelier1.checkersGameNutsAndBolts.PieceSquareColor.BLACK.equals(squareColor) ?
                GuiFactory.CASEBLACK : GuiFactory.CASEWHITE;
        this.setBackground(new Background(new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    public int getSquareid() {
        return squareid;
    }

    public static void main(String[] args) {

    }
}
