package checkersGameGui;


import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

import checkersGameController.Controller;
import atelier1.checkersGameNutsAndBolts.PieceSquareColor;

/**
 * @author francoiseperrin
 * <p>
 * Cette classe représente le damier de la vue
 * <p>
 * Elle tire les valeurs d'affichage d'une fabrique de constante
 * public final static int size = 10;
 * public final static double height = 600.0;
 *
 */
class Board extends GridPane {

	//region Attributs
	private final Controller controller;		// ... servira plus tard ...

	private int nbCol, nbLig;                	// le nb de ligne et de colonne du damier
	private double height;						// taille du damier en pixel
	private Canvas selectedPiece;

	private PieceListener pieceListener;
	private SquareListener squareListener;
	//endregion

	//region Getter & setters
	public Canvas getSelectedPiece() {
		return selectedPiece;
	}

	public void setSelectedPiece(Canvas selectedPiece) {
		System.out.println(this.selectedPiece);
		this.selectedPiece = selectedPiece;
		System.out.println(this.selectedPiece);
	}
	//endregion


	public Board (Controller controller) {
		super();

		this.nbCol = nbLig = GuiFactory.SIZE;
		this.height = (double) GuiFactory.HEIGHT;

		this.controller = controller;

		// initialisation du damier
		this.addSquaresOnCheckersBoard();
		this.addPiecesOnCheckersBoard();

		// initialisation du mouse listener
		pieceListener = new PieceListener();
		squareListener = new SquareListener();
	}


	private void addSquaresOnCheckersBoard () {

		Pane square = null;
		PieceSquareColor squareColor = null;

		for (int ligne = 0; ligne < this.nbLig; ligne++) {

			for (int col = 0; col < this.nbCol; col++) {

				// sélection de la couleur de la case
				if ((col % 2 == 0 && ligne % 2 == 0) || (col % 2 != 0 && ligne % 2 != 0)) {
					squareColor = PieceSquareColor.WHITE;
				} else {
					squareColor = PieceSquareColor.BLACK;
				}

				// création d'un Pane
				square = GuiFactory.createSquare(squareColor);
				square.setOnMouseClicked((e) ->{
					squareListener.handle(e);
				});


				// gestion de la taille des Pane
				square.setPrefHeight(this.height/this.nbLig);	// TODO - à remplacer atelier 4 : bad practice
				square.setPrefWidth(this.height/this.nbCol);	// TODO - à remplacer atelier 4 : bad practice

				// ajout du carre sur le damier
				this.add(square, col, ligne);

			}
		}
	}


	private void addPiecesOnCheckersBoard () {

		int blackIndex;
		int whiteIndex;

		// ajout pions noirs et blancs sur les cases noires des 4 lignes du haut et du bas
		// Rq : les index des carrés sur le damier varient de 0 à nbLig*nbLig-1 (=99)
		for (int j = 0; j < this.nbLig * 4; j += 2) {

			// recherche index du carré noir qui contient la pièce noire ou blanche
			if ((j / this.nbLig) % 2 == 0) {
				blackIndex = j + 1;
				whiteIndex = this.nbLig * this.nbLig - j - 2;
			} else {
				blackIndex = j;
				whiteIndex = this.nbLig * this.nbLig - j - 1;
			}

			// ajout effectif du pion noir puis du pion blanc sur les carrés identifiés
			addPieceOnSquare((Pane) this.getChildren().get(blackIndex), PieceSquareColor.BLACK);
			addPieceOnSquare((Pane) this.getChildren().get(whiteIndex), PieceSquareColor.WHITE);
		}

	}

	/**
	 * @param targetSquare
	 * @param pieceColor   Création d'une pièce et ajout dans le bon carré noir
	 */
	private void addPieceOnSquare (Pane targetSquare, PieceSquareColor pieceColor) {

		Canvas pieceGUI;

		// création de la pièce
		pieceGUI = GuiFactory.createPiece(pieceColor);
		pieceGUI.setOnMouseClicked((e) ->{
			pieceListener.handle(e);
		});

		// Ajout de la pièce sur le carré noir
		targetSquare.getChildren().add(pieceGUI);
	}

	private class PieceListener implements EventHandler<MouseEvent> {
		@Override
		public void handle (MouseEvent mouseEvent) {

			PieceGui piece = (PieceGui) mouseEvent.getSource();
			SquareGui square = (SquareGui) piece.getParent();
			int index = square.getSquareid();

			if(controller.isPieceMoveable(index))
			{
				Board.this.setSelectedPiece((Canvas) piece);
				mouseEvent.consume();
			}
		}
	}
	private class SquareListener implements EventHandler<MouseEvent> {
		@Override
		public void handle (MouseEvent mouseEvent) {
			PieceGui piece = (PieceGui) Board.this.getSelectedPiece();
			SquareGui square_init = (SquareGui) piece.getParent();
			SquareGui square_target = (SquareGui) mouseEvent.getSource();

			if(controller.isMoveTargetOk(square_init.getSquareid(),square_target.getSquareid())){
				Board.this.movePiece(Board.this.getSelectedPiece(), (Pane) square_target);
				mouseEvent.consume();
			}

			//Board.this.removePiece(square.getSquareid());
		}
	}

	public void removePiece(int tookPieceIndex){
		Pane square = (Pane)this.getChildren().get(tookPieceIndex);
		square.getChildren().clear();
	}

	private void movePiece (Canvas selectedPiece, Pane targetSquare){
		if(this.getSelectedPiece() == null)
			return;

		this.setSelectedPiece(null);
		targetSquare.getChildren().add(selectedPiece);
	}

}

/*
private class PieceListener implements EventHandler<MouseEvent> {
	@Override
	public void handle (MouseEvent mouseEvent) {
		Board.this.setSelectedPiece((Canvas) mouseEvent.getSource());
		mouseEvent.consume();
	}
}
private class SquareListener implements EventHandler<MouseEvent> {
	@Override
	public void handle(MouseEvent mouseEvent) {
		Board.this.movePiece(Board.this.getSelectedPiece(), (Pane) mouseEvent.getSource());
		mouseEvent.consume();
	}
}*/



