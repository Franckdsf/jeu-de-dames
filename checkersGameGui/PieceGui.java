package checkersGameGui;

import atelier1.checkersGameNutsAndBolts.PieceSquareColor;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

class PieceGui extends Canvas {

    public PieceGui(PieceSquareColor pieceColor){
        GraphicsContext graphicsContext = this.getGraphicsContext2D();

        // Gestion de la taille des Canvas 		// TODO - à remplacer atelier 4 : bad practice
        this.setHeight(GuiFactory.HEIGHT/GuiFactory.SIZE);
        this.setWidth(GuiFactory.HEIGHT/GuiFactory.SIZE);

        // la couleur est définie en dur
        Color color = Color.BLACK;
        if (pieceColor == PieceSquareColor.WHITE) {
            color = Color.WHITE;
        }
        graphicsContext.setFill(color);

        // calcul taille et position pièce en fonction du carré
        double rowWidth = this.getWidth();
        double rowHeight = this.getHeight();
        int offset = (int) ((rowWidth + rowHeight) / 6)   ;
        double width = rowWidth - offset;
        double height = rowHeight - offset;
        double upperLeftWidth = offset / 2;
        double upperLeftHeight = offset / 2;
        graphicsContext.fillArc(upperLeftWidth, upperLeftHeight, width, height, 30, 300, ArcType.ROUND);

    }

}
