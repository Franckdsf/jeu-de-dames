package checkersGameModel;

import java.util.Collection;

import checkersGameModel.Coord;
import atelier1.checkersGameNutsAndBolts.PieceSquareColor;

/**
 * @author francoise.perrin
 *
 * Cette classe gère les aspects métiers du jeu de dame
 * indépendement de toute vue
 * 
 * Elle stocke toutes les PieceModel dans une collection 
 * 
 */
public class Model  {

	private Collection<PieceModel> pieces;	// la collection de pièces en jeu - mélange noires et blanches
	private PieceSquareColor currentColor;	// couleur du joueur courant
	private int length;						// le nombre de lignes et colonnes du damier


	public Model() {
		super();

		this.length = ModelFactory.getlength();
		this.currentColor = ModelFactory.getBeginColor();
		this.pieces = ModelFactory.getPieceCollection();
		
		System.out.println(this);
	}

	/**
	 * @param coord
	 * @return true si la PieceModel qui se trouve aux coordonnées indiquées 
	 * est de la couleur du joueur courant 
	 */
	public boolean isPieceMoveable(Coord coord) {
		boolean bool  = true;
		
		// TODO
		
		return bool ;
	}


	/**
	 * @param initCoord
	 * @param targetCoord
	 * @return true si le déplacement est légal
	 * (s'effectue en diagonale, avec ou sans prise)
	 * La PieceModel qui se trouve aux coordonnées passées en paramètre 
	 * est capable de répondre à cette question
	 */
	public boolean isMovePieceOk(Coord initCoord, Coord targetCoord) {
		boolean bool  = true;
		
		// TODO
		
		return bool ;
	}

	/**
	 * @param initCoord
	 * @param targetCoord
	 * @return les coordonnées de la pièce à prendre
	 * après le déplacement effectif de la pièce
	 * et la prise éventuelle
	 */
	public Coord movePiece(Coord initCoord, Coord targetCoord) {

		Coord tookPieceCoord = null;

		// TODO
		
		return  tookPieceCoord;
	}

	public int getLength() {
		return this.length;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * 
	 * La méthode toStrong() retourne une représentation 
	 * de la liste de pièces sous forme d'un tableau 2D
	 * 
	 */
	public String toString() {

		String[][] damier = new String[this.length][this.length];

		// création d'un tableau 2D avec les noms des pièces
		for(PieceModel piece : this.pieces) {

			PieceSquareColor color = piece.getPieceColor();
			String stColor = (PieceSquareColor.WHITE.equals(color) ? "--B--" : "--N--" );

			int col = piece.getCoord().getColonne()-'a';
			int lig = piece.getCoord().getLigne() -1;
			damier[lig][col ] = stColor ;
		}

		// Affichage du tableau formatté
		String st = "     a      b      c      d      e      f      g      h      i      j\n";
		for ( int lig = 9; lig >=0 ; lig--) {
			st += (lig+1) + "  ";
			for ( int col = 0; col <= 9; col++) {					 
				String stColor = damier[lig][col];				
				if (stColor != null) {						
					st += stColor + "  ";	
				} 
				else {
					st += "-----  ";
				}
			}
			st +="\n";
		}
		return "Damier du model \n" + st;	
	}

	public static void main(String[] args) {
		
//		PieceModel pieceModel1 = new PawnModel(new Coord('b', 7), PieceSquareColor.BLACK);
//		System.out.println("pieceModel1 = " + pieceModel1);
//		pieceModel1.move(new Coord('a', 6));
//		System.out.println("pieceModel1 = " + pieceModel1);
		
		System.out.println("\n");
		Model model = new Model();
		System.out.println("findPieceModel ('b',4) = " + ModelFactory.findPiece(new Coord('b',4)));	// true
		System.out.println("findPieceModel ('b',6) = " + ModelFactory.findPiece(new Coord('b',6)));	// false
		System.out.println("isPieceMoveable ('b',4) = " + model.isPieceMoveable(new Coord('b',4)));	// true
		System.out.println("isPieceMoveable ('c',7) = " + model.isPieceMoveable(new Coord('c',7)));	// false
		
		System.out.println("isMovePieceOk ('b',4) -> ('c',5) = " + 
				model.isMovePieceOk(new Coord('b',4), new Coord('c',5)));	// true
		System.out.println("movePiece ('b',4) -> ('c',5) = " + 
				model.movePiece(new Coord('b',4), new Coord('c',5)));	// move OK
		
		System.out.println("\n");
		System.out.println("isPieceMoveable ('c',7) = " + model.isPieceMoveable(new Coord('c',7)));	// true
		System.out.println("isMovePieceOk ('c',7) -> ('c',6) = " + 
				model.isMovePieceOk(new Coord('c',7), new Coord('c',6)));	// false
		System.out.println("movePiece ('c',7) -> ('c',6) = " + 
				model.movePiece(new Coord('c',7), new Coord('c',6)));	// move KO
	}
}